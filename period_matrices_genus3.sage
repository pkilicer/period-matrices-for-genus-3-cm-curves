"""
Period_matrices_of_genus_3_CM_curves -- Sage package for computing 
period matrices of genus 3 CM curves

#*****************************************************************************
# Copyright (C) 2016 Marco Streng <marco.streng@gmail.com> and 
# Pinar Kilicer <pinarkilicer@gmail.com>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#*****************************************************************************

"""

"""

load_attach_mode(True,True)
load("https://bitbucket.org/mstreng/recip/raw/master/recip_online.sage")
# or:
load_attach_path("../../projects/recip")
load("recip.sage")

prec_bits = 3000
prec_digits = floor((log(2)/log(10))*prec_bits)

input_reduction_type="Minkowski_magma"
# or
input_reduction_type="no"
# or
input_reduction_type="LLL"

This is a modification of "recip" (which is GPL)
for sextic CM fields

"""

def period_matrix(bas, embs, use_magma=False):
    M = Matrix([[phi(b) for b in bas] for phi in embs])
    Omega1 = M[:,:3]
    Omega2 = M[:,3:]
    tau = Omega2.inverse()*Omega1
    tau_alt = (Omega1.inverse()*Omega2).inverse()
    a = max([abs(tau[i,j]-tau[j,i])/max(2, abs(tau[i,j])) for i in range(3) for j in range(3)])
    b = max([abs(tau_alt[i,j]-tau_alt[j,i])/max(2, abs(tau_alt[i,j])) for i in range(3) for j in range(3)])
    if a > 0.5:
        print("Ran into a Sage imprecision for matrix inverses :-O")
        print(RR(a))
    if a > b:
        tau = tau_alt
        a = b
    if a > 0.5:
        print(RR(a))
        raise RuntimeError("Could not circumvent the Sage imprecision for matrix inverses :-(")
    Y = Matrix([[t.imag() for t in u] for u in tau])
    if Y[0,0] < 0 or Y[0,0]*Y[1,1]-Y[1,0]*Y[0,1] < 0 or Y.determinant() < 0:
        raise RuntimeError("Matrix is not positive definite: " + str(Matrix(RR, Y)))

    if use_magma:
        tau_magma = (magma(Omega2)^-1*magma(Omega1)).sage()
        c = max([abs(tau_magma[i,j]-tau_magma[j,i])/max(2, abs(tau_magma[i,j])) for i in range(3) for j in range(3)])

        if c > 0.0000001:
            print("Magma also has some imprecision")
        if c < a:
            tau = tau_magma

    return tau


def split(bas):
    g = ZZ(bas.degree()/2)
    return vector(bas[:g]), vector(bas[g:])

def join(bas1,bas2):
    return vector(list(bas1)+list(bas2))

def B_to_M(B):
    g = B.nrows()
    assert B.ncols() == g
    return ABCD_to_M(identity_matrix(g), B, zero_matrix(g, g), identity_matrix(g))

def ABCD_to_M(A,B,C,D):
    g = A.ncols()
    for E in [A,B,C,D]:
        assert E.nrows() == E.ncols() == g
    return Matrix([join(A[i],B[i]) for i in range(A.nrows())] +
                  [join(C[i],D[i]) for i in range(A.nrows())])


def reduce_Siegel_epsilon(bas, embs, verbose=False, reduction_type="LLL"):
    """
    Given a symplectic basis and the embeddings from a CM-type,
    reduces to the domain F_S^epsilon of the notes:
    
      * Re(tau) has coeffs in [-1/2,1/2]
      * Im(tau) is reduced according to the reduction_type flag, which could be "LLL", "no" or "Minkowski_magma"
      * |tau[0,0]| >= 0.99

    returns a new basis and a basis transformation
    """
    input_bas = bas
    g = ZZ(bas.degree()/2)
    M = identity_matrix(2*g)
    
    while True:
        # the imaginary part:
        bas, N = reduce_imag_part(bas, embs, reduction_type, verbose=verbose)
        M = N * M
        assert act_by_M_on_basis(M, input_bas) == bas
        if verbose:
            print("imag reduction:" + str(N != identity_matrix(2*g)))
        # the real part:
        bas, N = reduce_real_part(bas, embs)
        M = N * M
        assert act_by_M_on_basis(M, input_bas) == bas
        if verbose:
            print("real reduction:" + str(N != identity_matrix(2*g)))
        # the upper left imaginary entry
        tau = period_matrix(bas, embs)
        if abs(tau[0,0]) < 0.99:
            N = M_for_reduce_using_z11(g)
            M = N * M
            bas = act_by_M_on_basis(N, bas)
            assert act_by_M_on_basis(M, input_bas) == bas
            if verbose:
                print("y11 reduction: yes")

        else:
            assert act_by_M_on_basis(M, input_bas) == bas
            if verbose:
                print("Finished!")
                print(" ======= ")
            return bas, M

def is_minkowski_reduced(Y, vector=False, verbose=False):
    """
    Given a Gram matrix Y, returns True if and only if Y is Minkowski reduced.

    Implementation: Uses Theorem 2.2 of Nguyen-Stehle - Low-dimensional lattice basis reduction revisited.

    If vector, then the output takes the form (b, c), where if b is False,
    then c is a pair (i, x) thatn contradicts the conditions in loc. cit.
    """
    g = ZZ(Y.ncols())
    assert Y.nrows() == g
    l = [[1,1],[1,1,1],[1,1,1,1],[1,1,1,1,1],[1,1,1,1,2],[1,1,1,1,1,1],[1,1,1,1,1,2],[1,1,1,1,2,2],[1,1,1,1,2,3]]
    l = [a[:g] for a in l]
    l = [a + [0 for i in range(g-len(a))] for a in l]
    G = SymmetricGroup(range(g))
    l = [[a[s(i)] for i in range(g)] for a in l for s in G]
    l = [[b[i]*a[i] for i in range(g)] for a in l for b in cartesian_product_iterator([[-1,1] for j in range(g)])]
    assert len(l) == 2^g*g.factorial()*9
    for i in range(g):
        for x in l:
            if gcd(x[i:]) == 1 and (Matrix([x])*Y*Matrix([x]).transpose())[0,0] < Y[i,i]:
                if vector:
                    return False, (i, x)
                return False
    if vector:
        return True, None
    return True



def reduce_real_part(bas, embs):
    """
    Given a symplectic basis and the embeddings from a CM-type,
    change the basis such that
    
      * Re(tau) has coeffs in [-1/2,1/2]
    
    returns a new basis and a basis transformation
    """
    tau = period_matrix(bas, embs)
    g = tau.ncols()
    B = Matrix(ZZ, [[0 for i in range(g)] for j in range(g)])
    for i in range(g):
        for j in range(g):
            c = - ZZ(tau[i,j].real().round())
            B[i,j] = c
            B[j,i] = c
            # TODO: maybe test whether tau is "sufficiently symmetric"
            # (tau should be symmetric, but rounding may make it non-symmetric)
    M = B_to_M(B)
    bas1, bas2 = split(bas)
    bas1 = bas1 + bas2*B
    bas_new = join(bas1, bas2)
    assert bas_new == bas * M.transpose()
    return bas_new, M


def reduce_imag_part(bas, embs, reduction_type, prec=None, verbose=False):
    """
    Given a symplectic basis and the embeddings from a CM-type,
    change the basis such that
    
      * Im(tau) is reduced according to reduction_type: "LLL", "Minkowski_magma" or "no"


    returns a new basis and a basis transformation

    f
    """
    g = ZZ(bas.degree() / 2)
    if reduction_type == "no":
        raise RuntimeError # (should not get here)
        #return bas, identity_matrix(2*g)
    if prec is None:
        prec = embs[0].codomain().precision()
    tau = period_matrix(bas, embs)
    Y = Matrix([[ZZ((2 ^ prec * t.imag()).round()) for t in u] for u in tau])
    # due to rounding, Y may be non-symmetric. Let's fix that:
    for i in range(g):
        for j in range(i):
            Y[i,j] = Y[j,i]
    if Y[0,0] < 0 or Y[0,0]*Y[1,1]-Y[1,0]*Y[0,1] < 0 or Y.determinant() < 0:
        raise ValueError("Matrix is not positive definite: " + str(Matrix(RR, Y)))


    if reduction_type == "LLL":
        U = Y.LLL_gram()
        Yr = U.transpose() * Y * U
        S = identity_matrix(g)
        print(Matrix([[a.sign() for a in b] for b in Yr]))
        for i in range(g - 1):
            if S[i, i] * Yr[i, i+1] < 0:
                S[i + 1, i + 1] = -1
        U = U * S
        print(S)

        Yr = U.transpose() * Y * U

        print(Matrix([[a.sign() for a in b] for b in Yr]))

        assert all([Yr[i,i+1]>=0 for i in range(g-1)])

        # Yr = U^T * Y * U is reduced
        # So want: M = (U^T  0 )
        #              ( 0  U^-1 )
        M = ABCD_to_M(U.transpose(), zero_matrix(g, g), zero_matrix(g, g), U.inverse())
        if verbose:
            print("After reduction, will be Minkowski-reduced as well as LLL-reduced: " + str(is_minkowski_reduced(U.transpose()*Y*U)))
        return bas*M.transpose(), M
    elif reduction_type == "Minkowski_magma":
        if verbose:
            print("Starting Magma for Minkowski reduction")
        try:
            (A, B) = magma(Y).MinkowskiGramReduction(nvals=2)
        except RuntimeError as e:
            Y = Matrix(RR, Y)
            print(Y)
            print(Y[0,0])
            print(Y.determinant())
            print(Y[0,0]*Y[1,1]-Y[1,0]*Y[0,1])
            print(Y - Y.transpose())

            raise e
        B = B.sage()
        if verbose:
            print("Finished with Magma")
        # B * Y * B^T is reduced? that's what the functions for which I could find documentation suggest
#        if verbose:
#            print "Reduced succesfully?"
#            print is_minkowski_reduced(B*Y*B.transpose())
#            print is_minkowski_reduced(B.transpose()*Y*B)
#            print is_minkowski_reduced(B.inverse()*Y*B.inverse().transpose())
#            print is_minkowski_reduced(B.inverse().transpose()*Y*B.inverse())
        assert is_minkowski_reduced(B*Y*B.transpose())
        M = ABCD_to_M(B, zero_matrix(g,g), zero_matrix(g,g), B.inverse().transpose())
    else:
        raise NotImplementedError
    return bas*M.transpose(), M


def M_for_reduce_using_z11(g):
    M = zero_matrix(2*g,2*g)
    M[0,g] = -1
    M[g,0] = 1
    for i in range(1,g):
        M[i,i] = 1
        M[g+i,g+i] = 1
    return M


def act_by_M_on_basis(M, bas):
    return bas*M.transpose()


def find_xi_g3(K, B):
    """
    OUTPUT: True, xi or False, None
    Returns a totally positive imaginary xi generating B if it exists.
    
    Assumes \OO*_K = W_K\OO*_F and assumes that \OO*_F takes all 8 signs.
    """
    assert B.is_principal()
    xi = B.gens_reduced()[0]
    u = K.unit_group()
    mu = K(u.torsion_generator())
    k = mu.multiplicative_order()
    for l in range(k/2):
        # test whether mu^l is totally imaginary:
        K0b, mb = K.subfield((mu**l*xi)**2, 'b')
        if K0b.degree() == len(K0b.embeddings(AA)) and (-K0b.gen()).is_totally_positive():
            xi = xi*mu**l
            return True, xi

    return False, None



def principally_polarized_ideal_classes(K):
    """
    Returns a set of principally polarized idealss (A, xi)
    that covers all principally polarizable ideal classes [A] exactly once.

        Note that Lemma 3.2.2 in Pinar's thesis says \OO*_K = W_K\OO*_F.
        So it is enough to multiply xi with a generator mu of W_K to get a
        totally imaginary generator.
    """
    lst = []
    c = K.complex_conjugation()
    D = K.different()
    for A in K.class_group_iter():
        # totally_real_cubic_subfield_and_embeddings(K)[0]
        #A = a.ideal()
        ideal_xi = (A*c(A)*D)**-1
        if ideal_xi.is_principal():
            b, xi = find_xi_g3(K, ideal_xi)
            if b:
                lst.append((xi, A))


    return lst


def imaginary_subfield_and_embedding(K):
    """
    Input: K = CM Field of degree 6
    Output: if K has an imaginary quadratic subfield k, then it returns
            a pair (k, m), where m is an embedding k --> K.
            Otherwise, returns None.

    """
    if K.degree() != 6:
        raise ValueError
#    a = K.automorphisms()
    subfields = K.subfields()
#    g = K.gen()
    for s in subfields:
        if s[0].degree() == 2:
            return s[0], s[1]
            

def primitive_CM_type(K, xi=None, prec=None):
    """
    Returns a primitive CM-type Phi of K.
    If xi is specified, then the CM-type has to satisfy phi(xi) / i > 0 in RR
    for all phi in Phi.
    
    If prec is not None, then do an extra numerical sanity check to double-check
    primitivity of the CM-type.
    """
    cc = K.complex_conjugation()
    k, embedding_k = imaginary_subfield_and_embedding(K)
    x0 = embedding_k(k.gen())
    x0 = x0 - cc(x0)


    if xi is None:
        a = K.gen()
        xi = a - cc(a)
        if K.is_totally_positive_real_element(xi/x0) or K.is_totally_positive_real_element(-xi/x0):
            # This xi does not define a primitive CM-type.
            # To fix this: multiply xi by a totally real element with not all signs equal.
            u = K(K.real_generator_in_self())
            u = u - u.trace() / 6
            assert u.trace() == 0
            # Now u has positive and negative conjugates.
            xi = u * xi
            assert not (K.is_totally_positive_real_element(xi/x0) or K.is_totally_positive_real_element(-xi/x0))

    else:


        if not K.is_totally_real_element(xi/x0):
            raise ValueError("Input xi is not totally positive imaginary")

        if K.is_totally_positive_real_element(xi/x0) or K.is_totally_positive_real_element(-xi/x0):
            raise ValueError("Input xi does not define a primitive CM-type")

    Phi = CM_Type(xi)

    if not prec is None:
        # here are some old tests, which are numerical only
        # and did not work
        embeds = Phi.embeddings(prec)
        if embeds[0](x0) == embeds[1](x0) == embeds[2](x0):
            raise RuntimeError("CM type is not primitive")

    return Phi



def period_matrices(K, prec, extra_info=False, reduction_type="LLL", verbose=False):
    """
    Chooses one primitive CM type, then computes one representative
    period matrix for every a.v. with CM by O_K of that CM type.
    
    Uses g=3. May yield duplicates if N_{K/K_0}(O_K^*) is not O_{K_0}^2.
    May miss something if O_K^* is not O_{K_0}^*?
    See  Lemma 3.2.2 in Pinar's thesis: should be ok in the cases we're looking at.
    """
    lst = []
    Phi = primitive_CM_type(K, prec=prec)
    K0 = K.real_field()
    [u0,u1,u2] = [K.real_to_self(K0(u)) for u in K0.unit_group().gens()]
    units = [1, u0, u1, u0*u1, u2, u0*u2, u1*u2, u0*u1*u2]
    embs = Phi.embeddings(prec)

    for (xi0, aaa) in principally_polarized_ideal_classes(K):
        for u in units:
            xi = u*xi0
            if Phi.is_totally_positive_imaginary(xi):
#                print [CC(phi(xi)) for phi in embs]
                bas = vector(_symplectic_basis(aaa, xi, K.complex_conjugation(), double_check=True))
#                print K, Phi, xi, aaa, bas
                tau_before_reduction = period_matrix(bas, embs)
                if reduction_type != "no":
                    (redbas, M) = reduce_Siegel_epsilon(bas, embs, verbose=verbose, reduction_type=reduction_type)
                else:
                    redbas = bas
                    M = identity_matrix(6)
#                bigmatrix = Matrix([[phi(b) for b in redbas] for phi in embs])
#                Omega1 = bigmatrix[:,:3]
#                Omega2 = bigmatrix[:,3:]
                try:
                    tau = period_matrix(redbas, embs, use_magma=True)
                except TypeError as e:
                    print(e)
                    tau = period_matrix(redbas, embs, use_magma=False)
#                Omega2.inverse()*Omega1
                if extra_info:
                    bigmatrix = Matrix([[phi(b) for b in redbas] for phi in embs])
                    lst.append((tau, aaa, xi0, u, xi, bas, redbas, M, bigmatrix))
                else:
                    lst.append(tau)
    return lst
